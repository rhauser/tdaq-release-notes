
# Nightly TDAQ Release

These are the evolving release notes for the next major TDAQ release.

```bash
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh nightly
mkdir work
cd work
cp /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/cmake/templates/CMakeLists.txt .
getpkg owl
mkdir build
cd build
cmake ..
make
make install
```

### Use of Python 2 based system commands

With the move to Python 3 inside the TDAQ software there is now a fundamental incompability between the normal system setup and the TDAQ environment.
If one wants to call a system command that is implemented in Python (2 for CentOS 7), like `yum` or `auth-get-sso-cookie` the environment has manipulated.

In most cases something like the following will be enough:

```shell
env -u PYTHONHOME -u PYTHONPATH auth-get-sso-cookie ...
```
If the command loads compiled libraries as well, it may be necessary to add a `-u LD_LIBRARY_PATH ` as well.
