# tdaq-12-00-00

[![Doxygen](https://img.shields.io/badge/Doxygen-12.0.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/doxygen/tdaq-12-00-00/html/index.html)
[![Javadoc](https://img.shields.io/badge/Javadoc-12.0.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-12-00-00/index.html)

The ATLAS TDAQ software version **`tdaq-12-00-00`** has been released
on December 16th, 2024.

## Availability and Installation

Outside of Point 1 the software should be used via CVMFS. It's official
location is:

    /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-12-00-00

At Point 1 the software is as usual available at

    /sw/atlas/tdaq/tdaq-12-00-00

The software can also be installed locally via [atlas-dnf5](https://gitlab.cern.ch/atlas-tdaq-software/atlas-dnf5) tool.
Pick the latest [release](https://gitlab.cern.ch/atlas-tdaq-software/atlas-dnf5/-/releases).
Prefer the `atlas-find-project` and `atlas-install-project` scripts over the plain `atlas-dnf5`
command. The former will automatically adjust the list of needed repositories for a given configuration.

## Configurations

The release is available for the following configurations:

  * x86_64-el9-gcc13-opt (default at Point 1)
  * x86_64-el9-gcc13-dbg (debug version at Point 1)
  * x86_64-el9-gcc14-opt (experimental)
  * x86_64-el9-gcc14-dbg (experimental)
  * aarch64-el9-gcc13-opt (experimental)
  * aarch64-el9-gcc14-opt (experimental)

All of of CentOS Stream, Rocky Linux, Alma Linux, RedHat Enterprise Linux are treated as equivalent.

## External Software

Note that this release uses CMake 3.31.0

### LCG_106b

The version of the external LCG software is [LCG_106b](http://lcginfo.cern.ch/release/106b/).

### TDAQ Specific External Software

 Package         | Version |  Requested by
 ----------------|---------|----------------
 czmq            | 4.2.1   |  FELIX
 zyre            | 2.0.1   |  FELIX
 colorama        | 0.4.4   |  DCS
 opcua           | 0.98.13 |  DCS
 fabric          | 3.2.2   |  TDAQ (PartitionMaker)
 pugixml         | 1.14    |  L1Calo, L1CTP
 ipbus-software  | 2.8.16  |  L1Calo, L1CTP
 microhttpd      | 1.0.1   |  TDAQ (pbeast)
 mailinglogger   | 5.1.0   |  TDAQ (SFO)
 Twisted         | 24.3.0  |  TDAQ (webis_server)
 urwid           | 2.1.2   |  TDAQ
 jwt-cpp         | v0.7.0  |  TDAQ
 Flask           | 2.1.3   |  TDAQ
 flask-sock      | 0.6.0   |  TDAQ (Phase II)
 gunicorn        | 21.2.0  |  TDAQ (Phase II)
 python-ldap     | 3.4.3   |  TDAQ (Phase II)
 CORAL           | 3.3.18  |  TDAQ
 COOL            | 3.3.19  |  TDAQ
 CrestApi        | rel-5.1-20241213-01 | (Phase II)
 hep-crest-client | 1.0.0  | (Phase II)
 pycrest-client  | 1.0.1   | (Phase II)

Note that `parallel-ssh` has been removed from the Python external package list
and replaced with `fabric` due to long-time inactivity in the upstream project,
leading to incompabilities with the latest Python versions.

## New and Removed Packages

The following packages have been added since tdaq-11-02-01:

  * pvss2pbeast

The following packages have been removed since tdaq-11-02-01:

  * DaqDbProxyUtils

## Setup Options

Assume you have an alias like:

```shell
alias cm_setup='source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh'
```

then

  * The `cm_setup --list` option will show the available releases including nightlies
  * The `cm_setup` script takes a short version of the CMTCONFIG build configuration
    as argument. E.g.
    * `cm_setup nightly dbg` will setup `x86_64-el9-gcc13-dbg`
    * `cm_setup nightly gcc13` will setup `x86_64-el9-gcc13-opt`
    * `cm_setup nightly gcc12-dbg` will setup `x86_64-el9-gcc13-dbg`
    * There is no short cut for setting the architecture or the OS
  * The `cm_setup --clean ...` option will bypass all testbed specific setup. This is
    useful if you want to use testbed hardware but be completely independent from the
    existing infrastructure. You have to set your own `TDAQ_IPC_INIT_REF` path to
    start a private initial partition, if needed.
  * `cm_setup --help` will show you all the options.

## Containers

See the [Container HOWTO](/HOWTOS/Containers) for instructions on how to use
the software in containers.
