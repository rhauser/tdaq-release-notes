#!/usr/bin/env python3
#
# usage: remove.py <limit> <release-note-filename>
#
# e.g. remove.py tdaq-09-02-00 RELEASE-NOTES.md
#
import sys
import re

def match2number(match):
   return int(match.group(2)) * 100 + int(match.group(3)) * 10 + int(match.group(4))

pattern = re.compile(' *## *tdaq-(common-)?(\\d\\d)-(\\d\\d)-(\\d\\d).*')

match = re.match("tdaq-(common-)?(\\d\\d)-(\\d\\d)-(\\d\\d)", sys.argv[1])
if match:
  limit = match2number(match)
else:
  print("Invalid release name")
  sys.exit(1)

for line in open(sys.argv[2]).readlines():
   match = pattern.match(line)
   if match:
     if match2number(match) <= limit:
        break
   print(line, end='')
