
# Introduction 

[![Doxygen](https://img.shields.io/badge/Doxygen-9.0.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/doxygen/tdaq-09-00-00/html/index.html)
[![Javadoc](https://img.shields.io/badge/Javadoc-9.0.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-09-00-00/index.html)


The ATLAS TDAQ software version **`tdaq-09-01-00`** has been released 
on 15th July 2020.

Please do not use tdaq-09-01-01, that will never go to Point 1 but is
only for having a Python 2 based build until off-line has officially switched.

## Availability and Installation

Outside of Point 1 the software should be used via CVMFS. It's official
location is 

    /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-01-00/

At Point 1 the software is as usual available at

    /sw/atlas/tdaq/tdaq-09-01-00/

The software
can also be installed locally via [ayum](https://gitlab.cern.ch/atlas-sit/ayum).

    git clone https://gitlab.cern.ch/atlas-sit/ayum.git
    source ayum/setup.sh

Modify the `prefix` entries in the yum repository files in `ayum/etc/yum.repos.d/*.repo`
to point to the desired destination.

    ayum install tdaq-09-01-00_x86_64-centos7-gcc8-opt

In case the LCG RPMs are not found, add this to etc/yum.repos.d/lcg.repo:

```
[lcg-repo-97]
name=LCG 97 Repository
baseurl=http://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/LCG_97a/
enabled=1
prefix=[...your prefix...]
```

## Configurations

The release is available for the following configurations:

* x86_64-centos7-gcc8-opt
* x86_64-centos7-gcc8-dbg

## External Software

### LCG_97apython3

The version of the external LCG software is [LCG_97apython3](http://lcginfo.cern.ch/release/97apython3/).

### TDAQ Specific External Software

 Package         | Version
 ----------------|---------
 cmzq            | 3.0.2
 zyre            | 1.1.0
 libfabric       | 1.6.2
 nlohmann/json   | 2.1.1
 pugixml         | 1.9
 ipbus-software  | 2.7.2
 microhttpd      | 0.9.59
 mailinglogger   | 5.0.0
 netifaces       | 0.10.9
 paramiko[gssapi]| 2.7.1


