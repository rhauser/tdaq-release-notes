This is the source for the [ATLAS TDAQ Releases](https://atlas-tdaq-sw-releases.web.cern.ch/) web page.

The content is either updated manually, or by adding new text to a package's `doc/RELEASE_NOTES.md`
file. The new content is taken into account when the affected package is submitted to the 
nightly release. A Gitlab CI job will automatically generate the latest version of 
the `nightly` release notes.

## Manual build of release notes

In top directory:

```shell
./bin/collect-relnotes --to=tdaq-09-04-00 tdaq-09-03-00
git status
git add docs/release-notes/tdaq-09-04-00.md
git commit
git push
```

