
# How to write release notes

There are two ways to add to the notes for a given release:

1. Update the `doc/RELEASE_NOTES.md` 
2. Create a custom documentation for a specific topic

## Per package release notes

If you want to keep per package release notes as in the old scheme, simply
add a `doc/RELEASE_NOTES.md` file. A script can collect these at any time.

The format should be as follows:

* a first level header with the package name
* a second level header with the name of an existing release as the
  first non-space word.

The collect script will be able to strip out older content with this
convention, and only copy the new stuff.

Example:

```
# MyPackage

Latest changes are:

  * Some change
  * Some other change

## tdaq-08-03-01

  * A change back in an old release

## tdaq-07-01-00 Production release 2017-2018

```

## General release notes

As you develop your code in the nightly release, and 
you have any news that a user of your software should
be aware of in the next release, make an entry for
your package/software subystem.

You should go [here](https://gitlab.cern.ch/atlas-tdaq-software/admin/tdaq-release-notes)
and fork the project into your personal group. In the following replace
`rhauser` with your CERN user name.

### Manual update and edits

```bash
git clone https://@gitlab.cern.ch:4434/rhauser/tdaq-release-notes.git
cd tdaq-release-notes
git remote add upstream https://@gitlab.cern.ch:4434/atlas-tdaq-sofware/admin/tdaq-release-notes.git
```

To edit locally (see below for how to preview):

```bash
git fetch upstream
git checkout -b mybranch upstream/master
cd docs/release-notes/nightly
vi MyPackage.md
```

When you are finished, commit your changes, push them to your private fork
and make a merge request from `mybranch` to the original project's `master` branch.

```bash
git add MyPackage.md
git commit 
git push origin mybranch
```

### Edit in Gitlab WebIDE

Another way is to keep your fork up-to-date and use gitlab to create a new branch,
and edit the files directly in WebIDE mode. The latter has a nice Markdown preview:

```bash
git checkout master
git pull upstream master
git push origin master
```

## Structure and Naming

Note that there is no requirement that the file(s) are 
exactly called like a package in the release.
It could be generic like `Monitoring.md` or 
`Readout.md`. You should conflict with an existing
package name.

Start the document with level 1 heading (which
will appear in the navigation bar):

```
# MyPackage

New features in MyPackage include:

* Feature A
* Feature B

```

If you want to enforce a certain order in the final output
name your files like `01-foo.md`, `02-bar.md` etc.

As new material is added to the package, an automatically updated view is  
[here](https://atlas-tdaq-sw-releases.web.cern.ch/).

## New TDAQ Release 

As a new TDAQ release is made, at some point the `nightly` directory is renamed
to e.g. `tdaq-09-01-00`, and a new empty nightly directory is made. The release notes
and the release itself are decoupled, you can still change the former without
putting a new tag into the release or nightly.

## Viewing the output

You might prefer to see a preview of how the page will look like. To get the
latest version of `mkdocs` on your CentOS 7 machine:

```
pip-3 install --user mkdocs
```

Unfortunately the tool insists of having various environment variables set properly.
If you don't want them in your normal environment, run a script like this:

```
#!/bin/bash
# run.mkdocs
LANG=en_US.utf-8 LC_ALL=en_US.utf-8 ~/.local/bin/mkdocs $@
```

In the top level directory of the package run (either `mkdocs` or `run.mkdocs`):

```
mkdocs serve
```

Point your browser to [http://localhost:8000](http://localhost:8000) to see the 
rendered pages as you edit the files.

## Installing the result

You don't need this as normal user, it will produce a directory `site` that you
can copy to any webserver that serves static files

```
mkdocs build
cp -r site /path/to/var/www/mysite
```

