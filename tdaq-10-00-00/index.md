
# tdaq-10-00-00

[![Doxygen](https://img.shields.io/badge/Doxygen-10.0.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/doxygen/tdaq-10-00-00/html/index.html)
[![Javadoc](https://img.shields.io/badge/Javadoc-10.0.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-10-00-00/index.html)


The ATLAS TDAQ software version **`tdaq-10-00-00`** has been released
on 6th January September 2022.

## Availability and Installation

Outside of Point 1 the software should be used via CVMFS. It's official
location is

    /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-10-00-00/

At Point 1 the software is as usual available at

    /sw/atlas/tdaq/tdaq-10-00-00/

The software
can also be installed locally via [ayum](https://gitlab.cern.ch/atlas-sit/ayum).

    git clone https://gitlab.cern.ch/atlas-sit/ayum.git
    source ayum/setup.sh

Modify the `prefix` entries in the yum repository files in `ayum/etc/yum.repos.d/*.repo`
to point to the desired destination.

    ayum install tdaq-10-00-00_x86_64-centos7-gcc11-opt

In case the LCG RPMs are not found, add this to etc/yum.repos.d/lcg.repo:

```
[lcg-repo-102b]
name=LCG 102b Repository
baseurl=http://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/LCG_102b/
enabled=1
prefix=[...your prefix...]
```

## Configurations

The release is available for the following configurations:

* x86_64-centos7-gcc11-opt (default at Point 1)
* x86_64-centos7-gcc11-dbg (debug version at Point 1)
* x86_64-centos9-gcc11-opt
* x86_64-centos9-gcc11-dbg
* x86_64-centos7-gcc12-opt (experimental)
* x86_64-centos7-gcc12-dbg (experimental)
* aarch64-centos7-gcc11-opt (experimental)

The CentOS 9 variant recognizes all of CentOS Stream, Rocky Linux,
Alma Linux, RedHat Enterprise Linux as equivalent. It is provided
since the plan is to upgrade the OS in Point 1 to Alma Linux 9
in the next winter shutdown (2023/24). It may become available
at Point 1 later this year as the new OS is tested.

## Microarchitecture

For the first time the TDAQ release is compiled with the
`-march=x86_64-v2` flag. This enables instructions up to
SSE 4.2 (but no AVX) and may be incompatible with older
Intel CPUs (approximately Nehalem or older). You will
see an illegal instruction exception on those machines.

Note that this option is the default on RedHat Enterprise 9.x
for all software.

## External Software

### LCG_102b

The version of the external LCG software is [LCG_102b](http://lcginfo.cern.ch/release/102b/).

### CORAL and COOL - 3.3.13

These two packages are no longer part of LCG. They are included in tdaq-common
and their use should be transparent for users.

The official targets to use are:

* CORAL::CoralBase
* CORAL::CoralServerBase
* CORAL::CoralMonitor
* CORAL::CoralStubs
* CORAL::CoralCommon
* CORAL::CoralServerProxy
* CORAL::CoralKernel
* CORAL::CoralSockets

### TDAQ Specific External Software

 Package         | Version |  Requested by
 ----------------|---------|----------------
 cmzq            | 4.2.1   |  FELIX
 zyre            | 2.0.1   |  FELIX
 libfabric       | 1.11.0  |  FELIX
 colorama        | 0.4.4   |  DCS
 opcua           | 0.98.13 |  DCS
 parallel-ssh    | 2.10.0  |  TDAQ (PartitionMaker)
 pugixml         | 1.9     |  L1Calo, L1CTP
 ipbus-software  | 2.8.4   |  L1Calo, L1CTP
 microhttpd      | 0.9.73  |  TDAQ (pbeast)
 mailinglogger   | 5.1.0   |  TDAQ (SFO)
 netifaces       | 0.11.0  |  TDAQ
 Twisted         | 22.4.0  |  TDAQ (webis_server)
 urwid           | 2.1.2   |  TDAQ
 jwt-cpp         | v0.6.0  |  TDAQ
 Flask           | 2.1.3   |  TDAQ

## Docker and Apptainer Images

The docker images used for building and testing the TDAQ software are
available here:

```shell
docker pull gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos7
docker pull gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos9
docker pull gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:aarch64-centos7
```

Run it like this:

```shell
docker run -it --rm -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos7
```

The corresponding apptainer/singularity images are here:

```
/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos7
/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos9
/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:aarch64-centos7
```

Run it like this:

```shell
apptainer shell -B /cvmfs /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos7
```

## Setup Options

In the following we assume some alias like

```shell
alias cm_setup='source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh'
```

  * The `cm_setup --list` option will show the available releases including nightlies
  * The `cm_setup --clean ...` option will bypass all testbed specific setup. This is
    useful if you want to use testbed hardware but be completely independent from the
    existing infrastructure. You have to set your own `TDAQ_IPC_INIT_REF` path to
    start a private initial partition, if needed.
  * The `cm_setup` script takes a short version of the CMTCONFIG build configuration
    as argument. E.g.
    * `cm_setup nightly dbg` will setup `x86_64-centos7-gcc11-dbg`
    * `cm_setup nightly gcc12` will setup `x86_64-centos7-gcc12-opt`
    * `cm_setup nightly gcc12-dbg` will setup `x86_64-centos7-gcc12-dbg`
    * There is no short cut for setting the architecture or the OS.
